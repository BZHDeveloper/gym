namespace Gym {
	public enum MappingStyle {
		ANY,
		BLOCK,
		FLOW
	}
	
	public interface Mapping : Gee.Map<Node, Node> {
		public abstract bool equal_to (Mapping mapping);
		
		public abstract MappingStyle style { get; set; }
	}
}
