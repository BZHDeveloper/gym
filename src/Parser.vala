namespace Gym {
	public errordomain ParserError {
		NULL,
		INVALID,
		LENGTH,
		NOT_FOUND,
		UNKNOWN
	}
	
	public interface Parser : GLib.Object {
		public abstract Gee.List<Document> load_all (string yaml) throws Error;
		
		public abstract Document load (string yaml) throws Error;
		
		public abstract Document load_first (string yaml) throws Error;
	}
}
