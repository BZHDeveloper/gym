namespace Gym {
	public abstract class AbstractVersionDirective : GLib.Object, VersionDirective {
		public abstract int major { get; set; }
		
		public abstract int minor { get; set; }
	}
}
