namespace Gym {
	public class GSequence : AbstractSequence {
		Gee.ArrayList<Node> nodes;
		
		construct {
			nodes = new Gee.ArrayList<Node>((a, b) => {
				return a.equal_to (b);
			});
		}
		
		public GSequence (SequenceStyle style = SequenceStyle.ANY) {
			GLib.Object (style : style);
		}
		
		public override bool add (Node node) {
			return nodes.add (node);
		}
		
		public override void clear() {
			nodes.clear();
		}
		
		public override bool contains (Node node) {
			return index_of (node) >= 0;
		}
		
		public override Node get (int index) {
			if (index < 0 || index >= nodes.size)
				return new GNode();
			return nodes[index];
		}
		
		public override int index_of (Node node) {
			return nodes.index_of (node);
		}
		
		public override void insert (int index, Node node) {
			if (index < 0 || index >= nodes.size)
				return;
			nodes.insert (index, node);
		}
		
		public override Gee.Iterator<Node> iterator() {
			return nodes.iterator();
		}
		
		public override Gee.ListIterator<Node> list_iterator() {
			return nodes.list_iterator();
		}
		
		public override bool remove (Node node) {
			return nodes.remove (node);
		}
		
		public override Node remove_at (int index) {
			if (index < 0 || index >= nodes.size)
				return new GNode();
			return nodes.remove_at (index);
		}
		
		public override void set (int index, Node node) {
			if (index < 0 || index >= nodes.size)
				return;
			nodes[index] = node;
		}
		
		public override Gee.List<Node>? slice (int start, int stop) {
			var seq = new GSequence();
			var list = nodes.slice (start, stop);
			if (list == null)
				return seq;
			for (var i = 0; i < list.size; i++)
				seq.add (list[i]);
			return seq;
		}
		
		public override SequenceStyle style { get; set; }
		
		public override bool read_only {
			get {
				return false;
			}
		}
		
		public override int size {
			get {
				return nodes.size;
			}
		}
	}
}
