namespace Gym {
	public class GDocument : AbstractDocument {
		public GDocument (bool start_implicit = true, bool end_implicit = true) {
			GLib.Object (start_implicit : start_implicit, end_implicit : end_implicit);
		}
		
		Gee.HashSet<TagDirective> hset;
		
		construct {
			hset = new Gee.HashSet<TagDirective>();
		}
		
		public override VersionDirective version_directive { get; set; }
		
		public override Gee.Set<TagDirective> tag_directives {
			get {
				return hset;
			}
		}
		
		public override bool start_implicit { get; set; }
		
		public override bool end_implicit { get; set; }
		
		public override Node root { get; set; }
		
		static bool my_write_func (void* user_data, uint8[] data) {
			try {
				OutputStream stream = (OutputStream)user_data;
				stream.write (data);
				return true;
			}
			catch {}
			return false;
		}
		
		Yaml.Document* yaml_document_new (Document document) {
			// ugly initialization, until we found better solution
			
			Yaml.Document* tmp = (Yaml.Document*)malloc (sizeof (Yaml.Document));
			var data = new Gee.ArrayList<uint8>();
			if (document.version_directive is VersionDirective) {
				var version = "%%YAML %d.%d\n".printf (document.version_directive.major, document.version_directive.minor);
				data.add_all_array (version.data);
			}
			foreach (var tag in document.tag_directives) {
				var str = "%%TAG %s %s\n".printf (tag.handle, tag.prefix);
				data.add_all_array (str.data);
			}
			if (data.size > 0)
				data.add_all_array ("---\n".data);
			var parser = Yaml.Parser();
			parser.initialize();
			parser.set_input_string (data.to_array());
			parser.load (tmp);
			Yaml.Document* doc = (Yaml.Document*)malloc (sizeof (Yaml.Document));
			doc->initialize (tmp->version_directive, tmp->tag_directives_start, tmp->tag_directives_end,
				document.start_implicit, document.end_implicit);
			return doc;
		}
		
		int yaml_document_add_scalar (Yaml.Document* doc, Node node) {
			var scalar = node.as_scalar();
			return doc->add_scalar (node.tag, scalar.value.data, (Yaml.ScalarStyle)scalar.style);
		}
		
		int yaml_document_add_sequence (Yaml.Document* doc, Node node) {
			var sequence = node.as_sequence();
			int seq = doc->add_sequence (node.tag, (Yaml.SequenceStyle)sequence.style);
			foreach (var child in sequence) {
				int item = yaml_document_add_node (doc, child);
				doc->append_sequence_item (seq, item);
			}
			return seq;
		}
		
		int yaml_document_add_mapping (Yaml.Document* doc, Node node) {
			var mapping = node.as_mapping();
			int map = doc->add_mapping (node.tag, (Yaml.MappingStyle)mapping.style);
			foreach (var entry in mapping.entries) {
				int key = yaml_document_add_node (doc, entry.key);
				int val = yaml_document_add_node (doc, entry.value);
				doc->append_mapping_pair (map, key, val);
			}
			return map;
		}
		
		int yaml_document_add_node (Yaml.Document* doc, Node node) {
			if (node.node_type == NodeType.SCALAR)
				return yaml_document_add_scalar (doc, node);
			if (node.node_type == NodeType.SEQUENCE)
				return yaml_document_add_sequence (doc, node);
			if (node.node_type == NodeType.MAPPING)
				return yaml_document_add_mapping (doc, node);
			var scalar = new GScalar();
			return yaml_document_add_scalar (doc, new GNode.from_scalar (scalar));
		}
		
		public override void dump_stream (OutputStream stream) throws Error {
			Yaml.Emitter emitter = Yaml.Emitter();
			emitter.initialize();
			emitter.set_output (my_write_func, stream);
			emitter.open();
			Yaml.Document* doc = yaml_document_new (this);
			if (root == null)
				throw new IOError.FAILED ("root node doesn't exist");
			yaml_document_add_node (doc, root);
			if (!emitter.dump (doc))
				throw new IOError.FAILED ("error during dump");
			emitter.close();
		}
	}
}
