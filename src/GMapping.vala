namespace Gym {
	public class GMapping : AbstractMapping {
		public class Pair : Gee.Map.Entry<Node, Node> {
			Node node_key;
			
			public Pair (Node key, Node val) {
				node_key = key;
				value = val;
			}
			
			public override Node key {
				get {
					return node_key;
				}
			}
			
			public override bool read_only {
				get {
					return false;
				}
			}
			
			public override Node value { get; set; }
		}
		
		Gee.ArrayList<Pair> pairs;
		
		construct {
			pairs = new Gee.ArrayList<Pair>((a, b) => {
				return a.key.equal_to (b.key);
			});
		}
		
		public GMapping (MappingStyle style = MappingStyle.ANY) {
			GLib.Object (style : style);
		}
		
		public override void clear() {
			pairs.clear();
		}
		
		public override Node get (Node key) {
			foreach (var pair in pairs)
				if (pair.key.equal_to (key))
					return pair.value;
			return new GNode();
		}
		
		public override bool has (Node key, Node value) {
			foreach (var pair in pairs)
				if (pair.key.equal_to (key) && pair.value.equal_to (value))
					return true;
			return false;
		}
		
		public override bool has_key (Node key) {
			foreach (var pair in pairs)
				if (pair.key.equal_to (key))
					return true;
			return false;
		}
		
		public override Gee.MapIterator<Node, Node> map_iterator() {
			var map = new Gee.HashMap<Node, Node>();
			foreach (var pair in pairs)
				map[pair.key] = pair.value;
			return map.map_iterator();
		}
		
		public override void set (Node key, Node value) {
			foreach (var pair in pairs)
				if (pair.key.equal_to (key)) {
					pair.value = value;
					return;
				}
			pairs.add (new Pair (key, value));
		}
		
		public override bool unset (Node key, out Node value = null) {
			value = new GNode();
			for (var i = 0; i < pairs.size; i++)
				if (pairs[i].key.equal_to (key)) {
					value = pairs[i].value;
					pairs.remove_at (i);
					return true;
				}
			return false;
		}
		
		public override MappingStyle style { get; set; }
		
		public override Gee.Set<Gee.Map.Entry<Node, Node>> entries {
			owned get {
				var hset = new Gee.HashSet<Gee.Map.Entry<Node, Node>>();
				foreach (var pair in pairs)
					hset.add (pair);
				return hset;
			}
		}
		
		public override Gee.Set<Node> keys {
			owned get {
				var hset = new NodeSet();
				foreach (var pair in pairs)
					hset.add (pair.key);
				return hset;
			}
		}
		
		public override Gee.Collection<Node> values {
			owned get {
				var list = new Gee.ArrayList<Node>();
				foreach (var pair in pairs)
					list.add (pair.value);
				return list;
			}
		}
		
		public override bool read_only {
			get {
				return false;
			}
		}
		
		public override int size {
			get {
				return pairs.size;
			}
		}
	}
}
