namespace Gym {
	public class NodeSet : Gee.AbstractSet<Node> {
		Gee.ArrayList<Node> nodes;
		
		construct {
			nodes = new Gee.ArrayList<Node>((a, b) => {
				return a.equal_to (b);
			});
		}
		
		public override bool add (Node node) {
			if (nodes.contains (node))
				return false;
			nodes.add (node);
			return true;
		}
		
		public override void clear() {
			nodes.clear();
		}
		
		public override bool contains (Node node) {
			return nodes.index_of (node) >= 0;
		}
		
		public override Gee.Iterator<Node> iterator() {
			return nodes.iterator();
		}
		
		public override bool remove (Node node) {
			int index = nodes.index_of (node);
			if (index < 0)
				return false;
			nodes.remove_at (index);
			return true;
		}
		
		public override bool read_only {
			get {
				return false;
			}
		}
		
		public override int size {
			get {
				return nodes.size;
			}
		}
	}
}
