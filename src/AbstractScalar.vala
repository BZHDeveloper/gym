namespace Gym {
	public abstract class AbstractScalar : GLib.Object, Scalar {
		public virtual bool equal_to (Scalar scalar) {
			return str_equal (value, scalar.value) && style == scalar.style;
		}
		
		public abstract string value { get; set; }
		
		public abstract ScalarStyle style { get; set; }
	}
}
