namespace Gym {
	public enum NodeType {
		NO,
		SCALAR,
		SEQUENCE,
		MAPPING
	}
	
	public interface Node : Gee.Hashable<Node> {
		public abstract NodeType node_type { get; }
		
		public abstract string tag { get; set; }
		
		public abstract Scalar as_scalar();
		
		public abstract Mapping as_mapping();
		
		public abstract Sequence as_sequence();
		
		public abstract void dump_stream (OutputStream stream) throws Error;
		
		public abstract string dump() throws Error;
		
		public abstract string to_string();
	}
}
