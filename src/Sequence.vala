namespace Gym {
	public enum SequenceStyle {
		ANY,
		BLOCK,
		FLOW
	}
	
	public interface Sequence : Gee.List<Node> {
		public abstract bool equal_to (Sequence sequence);
		
		public abstract SequenceStyle style { get; set; }
	}
}
