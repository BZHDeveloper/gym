namespace Gym {
	public abstract class AbstractNode : GLib.Object, Gee.Hashable<Node>, Node {
		public abstract NodeType node_type { get; }
		
		public abstract string tag { get; set; }
		
		public virtual bool equal_to (Node node) {
			if (node_type == NodeType.SCALAR)
				return as_scalar().equal_to (node.as_scalar());
			if (node_type == NodeType.SEQUENCE)
				return as_sequence().equal_to (node.as_sequence());
			if (node_type == NodeType.MAPPING)
				return as_mapping().equal_to (node.as_mapping());
			return node_type == NodeType.NO && node.node_type == NodeType.NO;
		}
		
		public virtual uint hash() {
			return str_hash (to_string());
		}
		
		public abstract Scalar as_scalar();
		
		public abstract Mapping as_mapping();
		
		public abstract Sequence as_sequence();
		
		public virtual void dump_stream (OutputStream stream) throws Error {
			var doc = new GDocument();
			doc.root = this;
			doc.dump_stream (stream);
		}
		
		public virtual string dump() throws Error {
			MemoryOutputStream mos = new MemoryOutputStream.resizable();
			dump_stream (mos);
			mos.close();
			return (string)mos.steal_data();
		}
		
		public virtual string to_string() {
			return dump();
		}
	}
}
