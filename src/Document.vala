namespace Gym {
	public interface Document : GLib.Object {
		public abstract VersionDirective version_directive { get; set; }
		
		public abstract Gee.Set<TagDirective> tag_directives { get; }
		
		public abstract bool start_implicit { get; set; }
		
		public abstract bool end_implicit { get; set; }
		
		public abstract Node root { get; set; }
		
		public abstract void dump_stream (OutputStream stream) throws Error;
		
		public abstract string dump() throws Error;
		
		public abstract string to_string();
	}
}
