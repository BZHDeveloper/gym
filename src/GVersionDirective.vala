namespace Gym {
	public class GVersionDirective : AbstractVersionDirective {
		public GVersionDirective (int major = 1, int minor = 1) {
			GLib.Object (major : major, minor : minor);
		}
		
		public override int major { get; set; }
		
		public override int minor { get; set; }
	}
}
