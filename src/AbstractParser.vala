namespace Gym {
	public abstract class AbstractParser : GLib.Object, Parser {
		public abstract Gee.List<Document> load_all (string yaml) throws Error;
		
		public virtual Document load (string yaml) throws Error {
			var list = load_all (yaml);
			if (list.size < 1)
				throw new ParserError.LENGTH ("no document found");
			if (list.size > 1)
				throw new ParserError.LENGTH ("extra documents found");
			return list[0];
		}
		
		public virtual Document load_first (string yaml) throws Error {
			var list = load_all (yaml);
			if (list.size < 1)
				throw new ParserError.LENGTH ("no document found");
			return list[0];
		}
	}
}

