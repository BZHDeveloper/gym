namespace Gym {
	public interface TagDirective : Gee.Hashable<TagDirective> {
		public abstract string handle { get; set; }
		
		public abstract string prefix { get; set; }
	}
}
