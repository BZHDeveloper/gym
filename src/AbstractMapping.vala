namespace Gym {
	public abstract class AbstractMapping : Gee.AbstractMap<Node, Node>, Mapping {
		public virtual bool equal_to (Mapping mapping) {
			if (size != mapping.size)
				return false;
			var a1 = keys.to_array();
			var a2 = mapping.keys.to_array();
			for (var i = 0; i < size; i++) {
				if (!a1[i].equal_to (a2[i]))
					return false;
				if (!this[a1[i]].equal_to (mapping[a2[i]]))
					return false;
			}
			return true;
		}
		
		public abstract MappingStyle style { get; set; }
	}
}

