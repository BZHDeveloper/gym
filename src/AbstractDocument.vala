namespace Gym {
	public abstract class AbstractDocument : GLib.Object, Document {
		public abstract VersionDirective version_directive { get; set; }
		
		public abstract Gee.Set<TagDirective> tag_directives { get; }
		
		public abstract bool start_implicit { get; set; }
		
		public abstract bool end_implicit { get; set; }
		
		public abstract Node root { get; set; }
		
		public abstract void dump_stream (OutputStream stream) throws Error;
		
		public virtual string dump() throws Error {
			MemoryOutputStream mos = new MemoryOutputStream.resizable();
			dump_stream (mos);
			mos.close();
			return (string)mos.steal_data();
		}
		
		public virtual string to_string() {
			return dump();
		}
	}
}
