namespace Gym {
	public class YamlParser : AbstractParser {
		Node load_scalar (Yaml.Node* node) {
			var s = new GScalar ((string)node->scalar_value, (ScalarStyle)node->scalar_style);
			var n = new GNode.from_scalar (s);
			if (node->tag != null)
				n.tag = (string)node->tag;
			return n;
		}
		
		Node load_sequence (Yaml.Node* node, Yaml.Document* doc) {
			var s = new GSequence ((SequenceStyle)node->sequence_style);
			for (int* item = node->sequence_items_start; item < node->sequence_items_top; item++) {
				var c = load_node (doc->get_node (*item), doc);
				s.add (c);
			}
			var n = new GNode.from_sequence (s);
			if (node->tag != null)
				n.tag = (string)node->tag;
			return n;
		}
		
		Node load_mapping (Yaml.Node* node, Yaml.Document* doc) {
			var m = new GMapping ((MappingStyle)node->mapping_style);
			for (Yaml.NodePair* pair = node->mapping_pairs_start; pair < node->mapping_pairs_top; pair++) {
				var key = load_node (doc->get_node (pair->key), doc);
				var val = load_node (doc->get_node (pair->value), doc);
				m[key] = val;
			}
			var n = new GNode.from_mapping (m);
			if (node->tag != null)
				n.tag = (string)node->tag;
			return n;
		}
		
		Node load_node (Yaml.Node* node, Yaml.Document* doc) {
			if (node->type == Yaml.NodeType.SCALAR_NODE)
				return load_scalar (node);
			if (node->type == Yaml.NodeType.MAPPING_NODE)
				return load_mapping (node, doc);
			if (node->type == Yaml.NodeType.SEQUENCE_NODE)
				return load_sequence (node, doc);
			return new GNode();
		}
		
		Document load_document (Yaml.Document* doc) {
			var document = new GDocument();
			if (doc->version_directive != null)
				document.version_directive = new GVersionDirective (doc->version_directive->major, doc->version_directive->minor);
			for (Yaml.TagDirective* tag = doc->tag_directives_start; tag != doc->tag_directives_end; tag++)
				document.tag_directives.add (new GTagDirective ((string)tag->handle, (string)tag->prefix));
			document.start_implicit = doc->start_implicit;
			document.end_implicit = doc->end_implicit;
			document.root = load_node (doc->get_root_node(), doc);
			return document;
		}
		
		public override Gee.List<Document> load_all (string yaml) throws Error {
			var list = new Gee.ArrayList<Document>();
			Yaml.Parser parser = Yaml.Parser();
			parser.initialize();
			parser.set_input_string (yaml.data);
			Yaml.Document* doc = (Yaml.Document*)malloc (sizeof(Yaml.Document));
			while (parser.load (doc)) {
				if (doc.get_root_node() == null)
					return list;
				list.add (load_document (doc));
			}
			// should not be reached
			throw new ParserError.UNKNOWN (parser.problem);
		}
	}
}
