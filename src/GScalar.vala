namespace Gym {
	public class GScalar : AbstractScalar {
		public GScalar (string value = "", ScalarStyle style = ScalarStyle.ANY) {
			GLib.Object (value : value, style : style);
		}
		
		public override ScalarStyle style { get; set; }
		
		public override string value { get; set; }
	}
}
