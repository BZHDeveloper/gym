namespace Gym {
	public interface VersionDirective : GLib.Object {
		public abstract int major { get; set; }
		
		public abstract int minor { get; set; }
	}
}
