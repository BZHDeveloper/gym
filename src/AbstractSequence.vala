namespace Gym {
	public abstract class AbstractSequence : Gee.AbstractList<Node>, Sequence {
		public virtual bool equal_to (Sequence sequence) {
			if (size != sequence.size)
				return false;
			for (var i = 0; i < size; i++)
				if (!this[i].equal_to (sequence[i]))
					return false;
			return true;
		}
		
		public abstract SequenceStyle style { get; set; }
	}
}
