namespace Gym {
	public class GTagDirective : AbstractTagDirective {
		public GTagDirective (string handle, string prefix) {
			GLib.Object (handle : handle, prefix : prefix);
		}
		
		public override string handle { get; set; }
		
		public override string prefix { get; set; }
	}
}
