namespace Gym {
	public class GNode : AbstractNode {
		Scalar scalar;
		Sequence sequence;
		Mapping mapping;
		
		public GNode.from_scalar (Scalar scalar) {
			this.scalar = scalar;
		}
		
		public GNode.from_sequence (Sequence sequence) {
			this.sequence = sequence;
		}
		
		public GNode.from_mapping (Mapping mapping) {
			this.mapping = mapping;
		}
		
		public override Scalar as_scalar() {
			if (scalar is Scalar)
				return scalar;
			return new GScalar();
		}
		
		public override Sequence as_sequence() {
			if (sequence is Sequence)
				return sequence;
			return new GSequence();
		}
		
		public override Mapping as_mapping() {
			if (mapping is Mapping)
				return mapping;
			return new GMapping();
		}
		
		public override string tag { get; set; }
		
		public override NodeType node_type {
			get {
				if (scalar is Scalar)
					return NodeType.SCALAR;
				if (sequence is Sequence)
					return NodeType.SEQUENCE;
				if (mapping is Mapping)
					return NodeType.MAPPING;
				return NodeType.NO;
			}
		}
	}
}
