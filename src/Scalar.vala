namespace Gym {
	public enum ScalarStyle {
		ANY,
		PLAIN,
		SINGLE_QUOTED,
		DOUBLE_QUOTED,
		LITERAL,
		FOLDED	
	}
	
	public interface Scalar : GLib.Object {
		public abstract bool equal_to (Scalar scalar);
		
		public abstract string value { get; set; }
		
		public abstract ScalarStyle style { get; set; }
	}
}
