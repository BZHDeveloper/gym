[CCode (cheader_filename = "yaml.h")]
namespace Yaml {
	[CCode (cname = "yaml_version_directive_t")]
	public struct VersionDirective {
		public int major;
		public int minor;
	}
	
	[CCode (cname = "yaml_tag_directive_t")]
	public struct TagDirective {
		public uint8* handle;
		public uint8* prefix;
	}
	
	[CCode (cname = "yaml_write_handler_t", has_target = false)]
	public delegate bool WriteFunc (void* user_data, [CCode (array_length_type = "size_t")]uint8[] data);
	[CCode (cname = "yaml_read_handler_t", has_target = false)]
	public delegate bool ReadFunc (void* user_data, [CCode (array_length_type = "size_t")]uint8[] data, out size_t read);
	
	[CCode (cname = "yaml_emitter_t", destroy_function = "yaml_emitter_delete")]
	public struct Emitter {
		public bool initialize();
		public void set_output_string ([CCode (array_length_type = "size_t")]out uint8[] data, out size_t written);
		public void set_output_file (GLib.FileStream file);
		public void set_output (Yaml.WriteFunc func, void* user_data);
		public bool open();
		public bool close();
		public bool dump (Yaml.Document* doc);
	}
	
	[CCode (cname = "yaml_document_t", destroy_function = "yaml_document_delete")]
	public struct Document {
		public bool initialize (Yaml.VersionDirective* d, Yaml.TagDirective* start, Yaml.TagDirective* end, bool start_implicit, bool end_implicit);
		public Yaml.Node* get_node (int index);
		public Yaml.Node* get_root_node();
		public int add_mapping (uint8* tag, Yaml.MappingStyle style);
		public int add_sequence (uint8* tag, Yaml.SequenceStyle style);
		public int add_scalar (uint8* tag, uint8[] value, Yaml.ScalarStyle style);
		public bool append_sequence_item (int sequence, int item);
		public bool append_mapping_pair (int mapping, int key, int value);
		
		public Yaml.VersionDirective* version_directive;
		[CCode (cname = "tag_directives.start")]
		public Yaml.TagDirective* tag_directives_start;
		[CCode (cname = "tag_directives.end")]
		public Yaml.TagDirective* tag_directives_end;
		public bool start_implicit;
		public bool end_implicit;
	}
	
	[CCode (cname = "yaml_parser_t", destroy_function = "yaml_parser_delete")]
	public struct Parser {
		public bool initialize();
		public bool load (Yaml.Document* doc);
		
		public void set_input_string ([CCode (array_length_type = "size_t")] uint8[] data);
		public void set_input_file (GLib.FileStream file);
		public void set_input (Yaml.ReadFunc func, void* user_data);
		
		public Yaml.ErrorType error;
		public string problem;
	}

	[CCode (cname = "yaml_node_pair_t")]
	public struct NodePair {
		public int key;
		public int value;
	}
	
	[CCode (cname = "yaml_node_t")]
	public struct Node {
		public Yaml.NodeType type;
		public uint8* tag;
		[CCode (cname = "data.scalar.value")]
		public uint8* scalar_value;
		[CCode (cname = "data.scalar.length")]
		public size_t scalar_length;
		[CCode (cname = "data.scalar.style")]
		public Yaml.ScalarStyle scalar_style;
		[CCode (cname = "data.sequence.items.start")]
		public int* sequence_items_start;
		[CCode (cname = "data.sequence.items.end")]
		public int* sequence_items_end;
		[CCode (cname = "data.sequence.items.top")]
		public int* sequence_items_top;
		[CCode (cname = "data.sequence.style")]
		public Yaml.SequenceStyle sequence_style;
		[CCode (cname = "data.mapping.pairs.start")]
		public Yaml.NodePair* mapping_pairs_start;
		[CCode (cname = "data.mapping.pairs.end")]
		public Yaml.NodePair* mapping_pairs_end;
		[CCode (cname = "data.mapping.pairs.top")]
		public Yaml.NodePair* mapping_pairs_top;
		[CCode (cname = "data.mapping.style")]
		public Yaml.MappingStyle mapping_style;
	}
	
	[CCode (cname = "yaml_node_type_t", cprefix = "YAML_")]
	public enum NodeType {
		NO_NODE,
		SCALAR_NODE,
		SEQUENCE_NODE,
		MAPPING_NODE
	}
	
	[CCode (cname = "yaml_error_type_t", cprefix = "YAML_")]
	public enum ErrorType {
		NO_ERROR,
		MEMORY_ERROR,
		READER_ERROR,
		SCANNER_ERROR,
		PARSER_ERROR,
		COMPOSER_ERROR,
		WRITER_ERROR,
		EMITTER_ERROR
	}
	
	[CCode (cname = "yaml_scalar_style_t", cprefix = "YAML_")]
	public enum ScalarStyle {
		ANY_SCALAR_STYLE,
		PLAIN_SCALAR_STYLE,
		SINGLE_QUOTED_SCALAR_STYLE,
		DOUBLE_QUOTED_SCALAR_STYLE,
		LITERAL_SCALAR_STYLE,
		FOLDED_SCALAR_STYLE
	}
	
	[CCode (cname = "yaml_sequence_style_t", cprefix = "YAML_")]
	public enum SequenceStyle {
		ANY_SEQUENCE_STYLE,
		BLOCK_SEQUENCE_STYLE,
		FLOW_SEQUENCE_STYLE
	}
	
	[CCode (cname = "yaml_mapping_style_t", cprefix = "YAML_")]
	public enum MappingStyle {
		ANY_MAPPING_STYLE,
		BLOCK_MAPPING_STYLE,
		FLOW_MAPPING_STYLE
	}
}
