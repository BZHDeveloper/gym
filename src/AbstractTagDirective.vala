namespace Gym {
	public abstract class AbstractTagDirective : GLib.Object, Gee.Hashable<TagDirective>, TagDirective {
		public virtual bool equal_to (TagDirective tag) {
			return str_equal (handle, tag.handle);
		}
		
		public virtual uint hash() {
			return str_hash (handle);
		}
		
		public abstract string handle { get; set; }
		
		public abstract string prefix { get; set; }
	}
}
